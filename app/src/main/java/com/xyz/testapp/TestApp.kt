package com.xyz.testapp

import android.app.Application
import com.xyz.testapp.auth.AuthRepo
import com.xyz.testapp.auth.AuthVMF
import com.xyz.testapp.network.APInterface
import com.xyz.testapp.network.NetworkConnectionInterceptor
import com.xyz.testapp.user.InspectionRepo
import com.xyz.testapp.user.InspectionVMF
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class TestApp : Application(), KodeinAware {

    /**
     * Kodein framework for dependency injection
     */
    override val kodein = Kodein.lazy {
        import(androidXModule(this@TestApp))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { APInterface(instance(), instance()) }

        bind() from singleton { AuthRepo(instance()) }
        bind() from provider { AuthVMF(instance()) }

        bind() from singleton { InspectionRepo(instance()) }
        bind() from provider { InspectionVMF(instance()) }

    }
}
