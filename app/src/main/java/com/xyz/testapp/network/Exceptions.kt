package com.xyz.testapp.network

import java.io.IOException
import java.net.SocketTimeoutException

/**
 * Common Exception file
 */


class APIExceptions(message: String): IOException(message)

class NoInternetException(message: String): IOException(message)