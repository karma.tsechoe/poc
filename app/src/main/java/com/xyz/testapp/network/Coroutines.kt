package com.xyz.testapp.network

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Handler for Network/Database using Coroutine
 */
object Coroutines {

    fun main(work: suspend (() -> Unit)){

        CoroutineScope(Dispatchers.Main).launch {
            work()
        }
    }
}