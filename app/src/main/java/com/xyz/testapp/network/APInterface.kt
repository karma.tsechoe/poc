package com.xyz.testapp.network

import android.app.Application
import android.content.Context
import com.xyz.testapp.utils.AUTH_TOKEN
import com.xyz.testapp.utils.Helper
import com.xyz.testapp.utils.MainURL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSession
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

interface APInterface {

    @Headers("Content-Type: application/json")
    @GET("asset/getassetforqr/{id}")
    suspend fun assetForQR(@Path("id") id: String): Response<String?>

    @Headers("Content-Type: application/json")
    @POST("user/operatorScanLogin")
    suspend fun operatorScanLogin(
            @Body body: String
    ): Response<String?>

    @Headers("Content-Type: application/json")
    @GET("checklist/byAccount/{id}")
    suspend fun checkList(@Path("id") accountId: String): Response<String?>

    @Headers("Content-Type: application/json")
    @GET("checklist/{id}")
    suspend fun checkDetail(@Path("id") accountId: String): Response<String?>

    @Headers("Content-Type: application/json")
    @POST("checklist/checklistResponse")
    suspend fun submitCheckResponse(
            @Body body: String
    ): Response<String?>

    companion object {
        operator fun invoke(context: Context, interceptor: NetworkConnectionInterceptor): APInterface {
            val okHttpClient = getOkHttpClient(context, interceptor)!!
            return Retrofit.Builder().client(okHttpClient)
                .baseUrl(MainURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).build()
                .create(APInterface::class.java)
        }


        fun getOkHttpClient(context: Context, interceptor: NetworkConnectionInterceptor): OkHttpClient? {
            //   ProviderInstaller.installIfNeeded(this)
            return try {
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts =
                    arrayOf<TrustManager>(
                        object : X509TrustManager {
                            override fun checkClientTrusted(
                                chain: Array<X509Certificate>,
                                authType: String
                            ) {
                            }

                            override fun checkServerTrusted(
                                chain: Array<X509Certificate>,
                                authType: String
                            ) {
                            }

                            override fun getAcceptedIssuers(): Array<X509Certificate> {
                                return arrayOf()
                            }
                        }
                    )
                // Install the all-trusting trust manager
                val sslContext = SSLContext.getInstance("TLSv1.2")
                sslContext.init(null, trustAllCerts, SecureRandom())
                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory
                val builder = OkHttpClient.Builder()
                builder.addInterceptor(interceptor)
                val authToken = Helper.getSharedPref(context, AUTH_TOKEN)
                if(!authToken.isNullOrEmpty()){
                    builder.addInterceptor { chain ->
                        val original = chain.request()
                        val request = original.newBuilder()
                            .header("auth-token", authToken)
                            .method(original.method(), original.body())
                            .build()
                        chain.proceed(request)
                    }
                }
                builder.readTimeout(3, TimeUnit.MINUTES)
                    .writeTimeout(3, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                builder.sslSocketFactory(sslSocketFactory)
                builder.hostnameVerifier { hostName: String?, session: SSLSession? -> true }
                builder.build()
            } catch (e: Exception) {
                e.printStackTrace()
                throw RuntimeException(e)
            }
        }
    }
}