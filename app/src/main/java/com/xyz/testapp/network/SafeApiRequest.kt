package com.xyz.testapp.network

import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import java.lang.StringBuilder

/**
 * Common class for Success & Failure response
 */
open class SafeApiRequest {

    suspend fun<T:Any?> apiRequest(call: suspend () -> Response<T>): T{

        val response = call.invoke()
        if(response.isSuccessful){
            return  response.body()!!
        }else{
            val error = response.errorBody()?.toString()

            val message = StringBuilder()
            error?.let {
                try {
                    message.append(JSONObject(it).getString("message"))
                }catch (e: JSONException){}
            }
            message.append("Error code is ${response.code()}")
            throw APIExceptions(message.toString())
        }
    }
}