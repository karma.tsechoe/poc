package com.xyz.testapp.user

import com.xyz.testapp.network.APInterface
import com.xyz.testapp.network.SafeApiRequest

/**
 * Repository class for Checklist get and post
 */
class InspectionRepo(private val api: APInterface): SafeApiRequest() {

    suspend fun checkList(id: String): String?{
       return apiRequest {
            api.checkList(id)
        }
    }

    suspend fun checkDetail(id: String): String?{
        return apiRequest {
            api.checkDetail(id)
        }
    }

    suspend fun submitCheckResponse(request: String): String?{
        return apiRequest {
            api.submitCheckResponse(request)
        }
    }
}