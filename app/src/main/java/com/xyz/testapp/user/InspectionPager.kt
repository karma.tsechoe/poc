package com.xyz.testapp.user

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.xyz.testapp.R


class InspectionPager(private val list: ArrayList<QuestionSet>): PagerAdapter() {


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == (`object` as? View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): View {
        val view = LayoutInflater.from(container.context).inflate(R.layout.inspection_item_layout, container, false)
        val textView : TextView = view.findViewById(R.id.text)

        textView.text = list[position].question

        container.addView(view)
        return view
    }
    override fun getCount(): Int = list.size

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as? View)
    }
}