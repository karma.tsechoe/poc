package com.xyz.testapp.user

interface CheckListener {

    fun onResultGet(list: List<CheckModel>?)
    fun onCheckModelGet(checkModel: CheckModel?)
    fun onSubmitComplete(message: String?)
}