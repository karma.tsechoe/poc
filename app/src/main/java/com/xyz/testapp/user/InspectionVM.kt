package com.xyz.testapp.user

import android.content.Context
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.xyz.testapp.auth.AssetModel
import com.xyz.testapp.network.APIExceptions
import com.xyz.testapp.network.Coroutines
import com.xyz.testapp.network.NoInternetException
import com.xyz.testapp.utils.ASSET_ID
import com.xyz.testapp.utils.ASSET_MODEL
import com.xyz.testapp.utils.Helper
import com.xyz.testapp.utils.OPERATOR_ID
import org.json.JSONException
import org.json.JSONObject

class InspectionVM(private val repo: InspectionRepo): ViewModel() {

    var listener: CheckListener? = null

    fun getCheckList(context: Context){
        Coroutines.main {
            try {
                val list = arrayListOf<CheckModel>()
                val assetString = Helper.getSharedPref(context, ASSET_MODEL)
                val assetResponse = Gson().fromJson(assetString, AssetModel::class.java)
                if (assetResponse != null) {

                    val response = repo.checkList(assetResponse.accountId._id)

                    response?.let {

                        val resultJSON = JSONObject(it)
                        val success = resultJSON.getBoolean("success")
                        if (success) {
                            val jsonArray = resultJSON.getJSONArray("data")
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject = jsonArray.getJSONObject(i)

                                val checkModel = Gson().fromJson(jsonObject.toString(), CheckModel::class.java)
                                list.add(checkModel)
                            }
                        }
                    }

                    listener?.onResultGet(list)
                }
            }catch (ae: APIExceptions){
                ae.printStackTrace()
            }catch (ie: NoInternetException){
                ie.printStackTrace()
            }
        }
    }

    fun getCheckDetail(id: String){

        Coroutines.main {
            try{

                val response = repo.checkDetail(id)

                response?.let {

                    val resultJSON = JSONObject(it)
                    val success = resultJSON.getBoolean("success")
                    if (success) {
                        val jsonObject = resultJSON.getJSONObject("data")
                        val checkModel = Gson().fromJson(jsonObject.toString(), CheckModel::class.java)
                        listener?.onCheckModelGet(checkModel)
                    }
                }
            }catch (ae: APIExceptions){
                ae.printStackTrace()
            }catch (ie: NoInternetException){
                ie.printStackTrace()
            }
        }
    }

    fun submitCheckResponse(jsonObject: JSONObject){
        Coroutines.main {
            try{
                val response = repo.submitCheckResponse(jsonObject.toString())
                response?.let {
                    val resultJSON = JSONObject(it)
                    if(resultJSON.has("success") && resultJSON.getBoolean("success")){
                        val msg = resultJSON.getString("message")
                        listener?.onSubmitComplete(msg)
                    }
                }
            }catch (je: JSONException){
                je.printStackTrace()
            }catch (ae: APIExceptions){
                ae.printStackTrace()
            }catch (ie : NoInternetException){
                ie.printStackTrace()
            }
        }
    }
}