package com.xyz.testapp.user

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.xyz.testapp.R
import com.xyz.testapp.auth.AssetActivity
import com.xyz.testapp.auth.user.MainActivity
import com.xyz.testapp.databinding.ActivityInspectionBinding
import com.xyz.testapp.utils.*
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

/**
 * Activity for Sections pre and post submit
 */
class InspectionActivity : AppCompatActivity(), KodeinAware, CheckListener, View.OnClickListener {

    override val kodein by kodein()
    private val factory: InspectionVMF by instance()
    private var checkModel: CheckModel? = null
    private var sectionCount: Int = 0
    private val requestJSON by lazy {
        val operatorId = Helper.getSharedPref(this, OPERATOR_ID)
        val assetId = Helper.getSharedPref(this, ASSET_ID)
        JSONObject().put("accountId", checkModel!!.accountId)
                .put("inspectorId", operatorId)
                .put("assetId", assetId)
                .put("checklistId", checkModel!!._id)
    }

    private val questionArray by lazy {
        JSONArray()
    }

    private lateinit var binding: ActivityInspectionBinding
    private lateinit var viewModel: InspectionVM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInspectionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        checkModel = intent?.getSerializableExtra("checkModel") as? CheckModel

        viewModel = ViewModelProvider(this, factory).get(InspectionVM::class.java)
        viewModel.listener = this

        binding.preIncludeLayout.continueBtn.setOnClickListener(this)
        binding.postIncludeLayout.newInspectionBtn.setOnClickListener(this)
        binding.postIncludeLayout.logoutBtn.setOnClickListener(this)

        updateTitle()
    }

    private fun updateTitle(){
        binding.preIncludeLayout.sectionMsg.text = msg()
    }
    private fun msg(): String?{
        return checkModel!!.section[sectionCount].notificationMessage
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_SECTION_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                if (checkModel != null) {
                    val questionSet = data?.getSerializableExtra("questionSet") as? HashMap<Int, String>

                    if (questionSet != null) {
                        for ((key, value) in questionSet) {
                            checkModel!!.section[sectionCount].questions.forEach {
                                val sectionModel = checkModel!!.section[sectionCount]
                                val question = sectionModel.questions[0].questionSet[key]

                                val questionObject = JSONObject()
                                questionObject.put("sectionId", sectionModel._id)

                                questionObject.put("question", question.question)
                                questionObject.put("failedResponse", question.failedResponse)
                                questionObject.put("response", value)
                                questionObject.put("id", question._id)

                                questionArray.put(questionObject)
                            }
                        }
                    }
                    isComplete()
                }
            }


        }
    }

    private fun isComplete() {
        sectionCount++


        if (sectionCount == checkModel!!.section.size) {

             binding.preIncludeLayout.root.visibility = View.GONE
             binding.postIncludeLayout.root.visibility = View.VISIBLE

             requestJSON.put("questionResponse", questionArray)
             viewModel.submitCheckResponse(requestJSON)

             sectionCount = 0
         }else{
            updateTitle()
        }
    }

    override fun onResultGet(list: List<CheckModel>?) {

    }

    override fun onCheckModelGet(checkModel: CheckModel?) {

    }


    override fun onSubmitComplete(message: String?) {

    //    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            binding.preIncludeLayout.continueBtn.id -> {
                if (checkModel != null) {
                    if (checkModel!!.section.size > 0) {
                        if (checkModel!!.section[sectionCount].questions.size > 0 && checkModel!!.section[sectionCount].questions[0].questionSet.size > 0) {
                            startActivityForResult(Intent(this, QuestionActivity::class.java).putExtra("section", checkModel!!.section[sectionCount]), REQUEST_SECTION_CODE)
                        }else{
                            isComplete()
                        }
                    }
                }
            }
            binding.postIncludeLayout.newInspectionBtn.id -> {
                startActivity(Intent(this, MainActivity::class.java))
                finishAffinity()
            }
            binding.postIncludeLayout.logoutBtn.id -> {
                startActivity(Intent(this, AssetActivity::class.java))
                finishAffinity()
            }
        }
    }
}