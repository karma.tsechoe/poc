package com.xyz.testapp.user

import java.io.Serializable

/**
 * Model class for CheckList
 */
data class CheckModel(val section: ArrayList<SectionModel>, val createdDate: String?,
val assets: ArrayList<String?>?, val _id: String, val checklistName: String,
val checklistDescription: String, val accountId: String?, val __v: Int?,
val language: String?, val status: Boolean?, val updatedAt: String?):Serializable