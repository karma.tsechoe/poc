package com.xyz.testapp.auth.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.xyz.testapp.R
import com.xyz.testapp.auth.AssetModel
import com.xyz.testapp.databinding.ActivityMainBinding
import com.xyz.testapp.user.*
import com.xyz.testapp.utils.ASSET_MODEL
import com.xyz.testapp.utils.Helper
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

/**
 * Activity for CheckList selection
 */
class MainActivity : AppCompatActivity(), View.OnClickListener, CheckListener, KodeinAware {

    override val kodein by kodein()
    private val factory: InspectionVMF by instance()
    private lateinit var binding: ActivityMainBinding
    private val checkList = arrayListOf<String>()
    private var checkModelList = listOf<CheckModel>()
    private lateinit var viewModel: InspectionVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

         viewModel = ViewModelProvider(this, factory).get(InspectionVM::class.java)
        viewModel.listener = this
        binding.startInspection.setOnClickListener(this)

        viewModel.getCheckList(this)
        checkList.add("Select")
        setData(checkList)
    }

    private fun setData(list: List<String>) {
        val adapter = ArrayAdapter<String>(this, R.layout.spinner_simple_text, list)
        binding.checklistSpn.adapter = adapter
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            binding.startInspection.id -> {
                checkDetail()
            }
        }
    }

    private fun checkDetail(){
        getCheckId()?.let {
            viewModel.getCheckDetail(it)
        }
    }
    private fun getCheckId(): String?{
        val checkName = binding.checklistSpn.selectedItem.toString()

        checkModelList.forEach {
            if(it.checklistName.equals(checkName, true)){
                return it._id
            }
        }
        return null
    }

    override fun onResultGet(list: List<CheckModel>?) {
        if (!list.isNullOrEmpty()) {

            checkModelList = list

            val assetString = Helper.getSharedPref(this, ASSET_MODEL)
            val assetResponse = Gson().fromJson(assetString, AssetModel::class.java)
            val assetCheckList = assetResponse.assetChecklists
            for (i in list.indices) {
                val check = list[i]
                assetCheckList.forEach {
                    if (it.checklistId.equals(check._id)) {
                        checkList.add(check.checklistName)
                    }
                }
            }
            setData(checkList)
        }
    }

    override fun onCheckModelGet(checkModel: CheckModel?) {
        startActivity(Intent(this, InspectionActivity::class.java).putExtra("checkModel", checkModel))

    }

    override fun onSubmitComplete(message: String?) {

    }
}