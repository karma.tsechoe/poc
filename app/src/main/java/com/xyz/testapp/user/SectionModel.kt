package com.xyz.testapp.user

import java.io.Serializable

/**
 * Model class of Section
 */
data class SectionModel (val _id: String?, val sectionName: String?, val sectionDescription: String?,
val sectionStartNotification: Boolean?, val notificationMessage: String?, val questions: ArrayList<Question>): Serializable

data class Question(val questionSet: ArrayList<QuestionSet>, val _id: String?): Serializable

data class QuestionSet(val response: ArrayList<String>, val _id: String?,
val isMeterReading: Boolean, val failedResponse: Boolean, val question: String?,
val sectionId: String?, val __v: Int?, val updatedAt: String?): Serializable
