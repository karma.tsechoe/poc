package com.xyz.testapp.user

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.xyz.testapp.network.APInterface

class InspectionVMF(private val repo: InspectionRepo): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return InspectionVM(repo) as T
    }
}