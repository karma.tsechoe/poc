package com.xyz.testapp.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.get
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.xyz.testapp.R
import com.xyz.testapp.databinding.ActivityQuestionBinding
import com.xyz.testapp.view.PinView

/**
 * Activity class for Questions
 */
const val TAG = "QuestionActivity"

class QuestionActivity : AppCompatActivity(), View.OnClickListener {


    private lateinit var binding: ActivityQuestionBinding
    private lateinit var sectionModel: SectionModel
    private var list = arrayListOf<QuestionSet>()
    private lateinit var view: View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQuestionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sectionModel = intent?.getSerializableExtra("section") as SectionModel

        /* binding.previousBtn.setOnClickListener(this)
         binding.nextBtn.setOnClickListener(this)*/

        binding.responseView.setOnInflateListener(inflateListener)
        view = binding.responseView.inflate()



        list = sectionModel.questions[0].questionSet
        binding.viewPager.adapter = InspectionPager(list)

        binding.viewPager.addOnPageChangeListener(pagerListener)

    }

    private val inflateListener = ViewStub.OnInflateListener { stub, inflatedId ->
        responseView(inflatedId = inflatedId)
    }

    override fun onResume() {
        super.onResume()
        //Response Init
        val type = sectionModel.questions[0].questionSet[0].response[0]
        setResponseView(type)
    }

    private val pagerListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
        ) {

        }

        override fun onPageSelected(position: Int) {
            val type = sectionModel.questions[0].questionSet[position].response[0]
            responseView(type, view)
        }
    }

    private val answerHash by lazy {
        hashMapOf<Int, String?>()
    }

    private fun addAnswer(id: Int, answer: String?) {
        answerHash[id] = answer
    }

    private fun setResponseView(type: String) {
        Log.i(TAG, type)

        when (type) {
            "YES/NO/NA", "OK/NOT OK/NA", "Yes" -> {
                (view as? ViewGroup)?.getChildAt(0)?.visibility = View.VISIBLE
                (view as? ViewGroup)?.getChildAt(1)?.visibility = View.GONE
                (view as? ViewGroup)?.getChildAt(2)?.visibility = View.GONE
            }
            "NUMERIC" -> {
                (view as? ViewGroup)?.getChildAt(0)?.visibility = View.GONE
                (view as? ViewGroup)?.getChildAt(1)?.visibility = View.VISIBLE
                (view as? ViewGroup)?.getChildAt(2)?.visibility = View.GONE
            }
            "TEXT" -> {
                (view as? ViewGroup)?.getChildAt(0)?.visibility = View.GONE
                (view as? ViewGroup)?.getChildAt(1)?.visibility = View.GONE
                (view as? ViewGroup)?.getChildAt(2)?.visibility = View.VISIBLE

            }
        }
    }

    private fun responseView(type: String = "", inflatedId: View) {
        val yes: ImageView = inflatedId.findViewById<View>(R.id.include_yes_no_layout).findViewById(R.id.select_up)
        val no: ImageView = inflatedId.findViewById<View>(R.id.include_yes_no_layout).findViewById(R.id.select_down)
        val na: FrameLayout = inflatedId.findViewById<View>(R.id.include_yes_no_layout).findViewById(R.id.select_na)

        yes.setOnClickListener {
            addAnswer(binding.viewPager.currentItem, "Yes")
            submit()
        }
        no.setOnClickListener {
            addAnswer(binding.viewPager.currentItem, "No")
            submit()
        }
        na.setOnClickListener {
            addAnswer(binding.viewPager.currentItem, "NA")
            submit()
        }

        val pinView: PinView = inflatedId.findViewById<View>(R.id.include_numeric_layout).findViewById(R.id.pin)
        val submit: ImageView = inflatedId.findViewById<View>(R.id.include_numeric_layout).findViewById(R.id.select_up)

        if(pinView.value.trim().isNotEmpty())
            pinView.clearValue()

        submit.setOnClickListener {
            addAnswer(binding.viewPager.currentItem, pinView.value)
            submit()
        }
        val naPin: FrameLayout = inflatedId.findViewById<View>(R.id.include_numeric_layout).findViewById(R.id.select_na)
        naPin.setOnClickListener {
            addAnswer(binding.viewPager.currentItem, "NA")
            submit()
        }

        val responseEdit: EditText = inflatedId.findViewById<View>(R.id.include_text_layout).findViewById(R.id.edit_response)
        val responseSubmit: ImageView = inflatedId.findViewById<View>(R.id.include_text_layout).findViewById(R.id.select_up)

        responseSubmit.setOnClickListener {
            addAnswer(binding.viewPager.currentItem, responseEdit.text.toString().trim())
            submit()
        }

        setResponseView(type)
    }

    private fun submit() {
        if (!isComplete())
            moveNext()
        else {
            val intent = Intent()
            intent.putExtra("questionSet", answerHash)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }


    private fun isComplete(): Boolean {
        return answerHash.size == list.size
    }

    private fun moveNext() {
        if (binding.viewPager.currentItem < list.size - 1) {
            val pos = binding.viewPager.currentItem + 1
            binding.viewPager.setCurrentItem(pos, true)
        }
    }

    override fun onClick(p0: View?) {
        var pos = binding.viewPager.currentItem
        when (p0?.id) {
            binding.previousBtn.id -> {
                if (pos > 0) {
                    pos -= 1
                } else {
                    pos = list.size - 1
                }
                binding.viewPager.setCurrentItem(pos, true)
            }
            binding.nextBtn.id -> {
                if (pos < list.size) {
                    pos += 1
                } else {
                    pos = 0
                }
                binding.viewPager.setCurrentItem(pos, true)
            }
        }
    }
}