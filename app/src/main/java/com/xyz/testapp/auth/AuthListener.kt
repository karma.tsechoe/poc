package com.xyz.testapp.auth

interface AuthListener {

    fun onAsset(assetModel: AssetModel?)
    fun onOperatorSignIn(result: String?)
    fun onFailure(message: String?)
}