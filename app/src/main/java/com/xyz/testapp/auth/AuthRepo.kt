package com.xyz.testapp.auth

import com.xyz.testapp.network.APInterface
import com.xyz.testapp.network.SafeApiRequest

/**
 * Repository class for Asset and Operator
 */
class AuthRepo(private val api: APInterface): SafeApiRequest() {

    suspend fun assetForQR(id: String): String?{
        return apiRequest {
            api.assetForQR(id)
        }
    }
    suspend fun operatorSignIn(request: String): String?{
        return apiRequest {
            api.operatorScanLogin(request)
        }
    }
}