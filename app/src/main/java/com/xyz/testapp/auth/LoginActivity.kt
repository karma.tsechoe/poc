package com.xyz.testapp.auth

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import com.xyz.testapp.auth.user.MainActivity
import com.xyz.testapp.databinding.ActivityLoginBinding
import com.xyz.testapp.utils.REQUEST_CAMERA_PERMISSION
import com.xyz.testapp.utils.SCANNER_CODE
import com.xyz.testapp.utils.ScannerActivity
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.IOException

/**
 * Login Activity
 */
class LoginActivity : AppCompatActivity(), View.OnClickListener {


    private lateinit var binding : ActivityLoginBinding

    private var assetModel: AssetModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backImg.setOnClickListener(this)
        binding.scanner.setOnClickListener(this)

        assetModel = intent?.extras?.get("assetModel") as? AssetModel

        if(assetModel != null) {
            binding.assetName.text = assetModel?.assetName
            binding.assetId.text = assetModel?.assetUId
        }
    }

    private fun direct(){
        startActivity(Intent(this, ScannerActivity::class.java).putExtra("id", "badge"))
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            binding.backImg.id ->{
                finish()
            }
            binding.scanner.id ->{
                direct()
            }
        }
    }

}