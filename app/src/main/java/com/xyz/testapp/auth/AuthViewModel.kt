package com.xyz.testapp.auth

import android.content.Context
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.zxing.Result
import com.xyz.testapp.network.APIExceptions
import com.xyz.testapp.network.Coroutines
import com.xyz.testapp.network.NoInternetException
import com.xyz.testapp.utils.ASSET_ID
import com.xyz.testapp.utils.ASSET_MODEL
import com.xyz.testapp.utils.AUTH_TOKEN
import com.xyz.testapp.utils.Helper
import org.json.JSONException
import org.json.JSONObject

/**
 * View model for Authentication Module
 */
class AuthViewModel(private val repo: AuthRepo): ViewModel() {

    var listener: AuthListener? = null
    fun assetForQR(context: Context, result: Result) = Coroutines.main {

            try {
                val response = repo.assetForQR(result.toString())

                response?.let {

                    val resultJSON = JSONObject(it)
                    val assetString = resultJSON.getString("data")

                    Helper.saveSharedPref(context, ASSET_MODEL, assetString)
                    val assetResponse =
                            Gson().fromJson(assetString, AssetModel::class.java)
                    listener?.onAsset(assetResponse)
                }

            }catch (ae: APIExceptions){
                ae.printStackTrace()
                listener?.onFailure(ae.message)
            }catch (ie: NoInternetException){
                ie.printStackTrace()
                listener?.onFailure(ie.message)
            }catch (e: Exception){
                e.printStackTrace()
                listener?.onFailure(e.message)
            }
        }


    fun operatorSignIn(context: Context, result: Result) = Coroutines.main {
          try {
              val assetString = Helper.getSharedPref(context, ASSET_MODEL )
              val assetResponse = Gson().fromJson(assetString, AssetModel::class.java)
              if(assetResponse != null) {
                  try {
                      val requestJSON = JSONObject()
                      requestJSON.put("accountId", assetResponse.accountId._id)
                      requestJSON.put("siteId", assetResponse.siteId)
                      requestJSON.put("operatorId", result.toString())
                      val response = repo.operatorSignIn(requestJSON.toString())

                      response?.let {
                          val resultJSON = JSONObject(it)
                          val success = resultJSON.getBoolean("success")
                          if(success){
                            val token = resultJSON.getString("token")
                              Helper.saveSharedPref(context, AUTH_TOKEN, token)
                              val message = resultJSON.getString("message")
                              listener?.onOperatorSignIn(message)
                          }

                      }
                  }catch (e: JSONException){
                      e.printStackTrace()
                  }

              }
          }catch (ae: APIExceptions){
              ae.printStackTrace()
              listener?.onFailure(ae.message)
          }catch (ie: NoInternetException){
              ie.printStackTrace()
              listener?.onFailure(ie.message)
          }catch (e: Exception){
              e.printStackTrace()
              listener?.onFailure(e.message)
          }

    }
}