package com.xyz.testapp.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.xyz.testapp.databinding.ActivityAssetBinding
import com.xyz.testapp.utils.ScannerActivity

/**
 * Asset Activity
 */
class AssetActivity : AppCompatActivity(){

    private lateinit var binding: ActivityAssetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAssetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.scanner.setOnClickListener(onClickListener)
    }

    private val onClickListener = View.OnClickListener {
        direct()
    }

    private fun direct(){
        startActivity(Intent(this, ScannerActivity::class.java).putExtra("id", "asset"))
    }


}