package com.xyz.testapp.auth

import java.io.Serializable

/**
 * Model class of Asset Response
 */
data class AssetModel (val AssetStatus: Boolean, val _id: String,
val assetName: String, val assetUId: String, val accountId: Account,
val siteId: String, val assetChecklists: ArrayList<CheckList>, val assetImage: String): Serializable


data class Account(val _id: String, val accountNo: Int): Serializable
data class CheckList(val _id: String, val isDefaultChecklist: Boolean, val checklistId: String): Serializable