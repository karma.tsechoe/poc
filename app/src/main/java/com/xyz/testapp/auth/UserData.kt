package com.xyz.testapp.auth

import java.io.Serializable

/**
 * Model class of User response
 */
data class UserData(val fullName: String, val email: String, val password: String,
                    val permissions: String, val isOwner: Boolean, val isEmailConfirmed: Boolean,
val authorizedSites: Array<String?>, val certifiedVehicles: Array<String?>, val safetyScore: Int,
val productivity: Int, val stops: Int, val overrides: Int, val averageSpeed: Int, val imgData: String,
val pin: String, val createdDate: String, val _id: String, val accountId: String, val accountNo: Int,
val status: Boolean, val __v: Int): Serializable
