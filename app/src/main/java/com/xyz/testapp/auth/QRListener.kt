package com.xyz.testapp.auth

interface QRListener {

    fun onConfirm(data: String?)
    fun onCancel()
}