package com.xyz.testapp.utils

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.xyz.testapp.R
import com.xyz.testapp.auth.*
import com.xyz.testapp.auth.user.MainActivity
import com.xyz.testapp.databinding.ActivityScannerBinding
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.IOException

/**
 * Common Scanning Activity for both Asset & User Login
 */
class ScannerActivity : AppCompatActivity(), KodeinAware, AuthListener, View.OnClickListener {

    override val kodein by kodein()
    private val factory : AuthVMF by instance()
    private var viewModel : AuthViewModel? = null

    private lateinit var binding: ActivityScannerBinding
    private lateinit var codeScanner: CodeScanner

    private var id: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityScannerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.backImg.setOnClickListener(this)

        id = intent?.getStringExtra("id")

        when(id){
            "asset" -> binding.backMsg.text = getString(R.string.scan_asset_qr)
            "badge" -> binding.backMsg.text = getString(R.string.scan_badge_qr)
            else -> binding.backMsg.text = getString(R.string.scan_qr)
        }

        viewModel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)
        viewModel?.listener = this

        codeScanner = CodeScanner(this, binding.codeScanner)

        codeScanner.decodeCallback = decodeCallback
        codeScanner.errorCallback  = errorCallback

    }

    private fun scan(){
        try{
            if(ActivityCompat.checkSelfPermission(this@ScannerActivity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                preview()
            }else{
                ActivityCompat.requestPermissions(this@ScannerActivity, Array<String>(1){ Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION)
            }
        }catch (e: IOException){
            e.printStackTrace()
        }
    }

    private fun preview() =  codeScanner.startPreview()

    private val decodeCallback = DecodeCallback {
        when (id) {
            "asset" -> {
                Helper.saveSharedPref(this, ASSET_ID, it.toString())
                viewModel?.assetForQR(this@ScannerActivity, it)
            }
            "badge" ->{
                Helper.saveSharedPref(this, OPERATOR_ID, it.toString())
                viewModel?.operatorSignIn(this@ScannerActivity, it)}
        }
    }
    private val errorCallback = ErrorCallback{
        runOnUiThread {
            Toast.makeText(this, "Camera initialization error: ${it.message}",
                Toast.LENGTH_LONG).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == REQUEST_CAMERA_PERMISSION){
            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                preview()
            }
        }
    }

    override fun onResume() {
        super.onResume()
      scan()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    override fun onAsset(assetModel: AssetModel?) {
        startActivity(Intent(this, LoginActivity::class.java).putExtra("assetModel", assetModel))
    }

    override fun onOperatorSignIn(result: String?) {
        if(!result.isNullOrEmpty()){
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        }
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onFailure(message: String?) {
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        codeScanner.releaseResources()
        preview()
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            binding.backImg.id ->{
                finish()
            }
        }
    }
}