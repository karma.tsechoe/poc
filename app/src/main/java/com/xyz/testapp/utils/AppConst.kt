package com.xyz.testapp.utils

/**
 * Constants class of Project
 **/
const val REQUEST_CAMERA_PERMISSION: Int = 100
const val SCANNER_CODE = 101
const val MainURL = "https://dev-api.siera.ai/apiv1/"
const val SHARED_PREF = "SIERA_Shared"
const val AUTH_TOKEN = "auth_token"
const val ASSET_ID = "asset_id"
const val OPERATOR_ID = "operator_id"
const val ASSET_MODEL = "asset_model"

const val REQUEST_SECTION_CODE = 101