package com.xyz.testapp.utils

import android.content.Context

/**
 * Common BU logic class
 * Share Preference to store data locally
 */
class Helper {

    companion object{

        fun getSharedPref(context: Context, key: String): String?{
            val sharedPreferences = context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
            return sharedPreferences.getString(key, null)
        }

        fun saveSharedPref(context: Context, key: String, value: String){
            val sharedPreferences = context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(key, value)
            editor.apply()
        }
    }
}