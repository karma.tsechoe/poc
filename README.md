# PoC

Siera Sample Android Application:

1. This sample application helps in collecting reviews of operators on various checklist of selected assets.
2. Barcode is used as primary means to identify and collect the asset and operator details.


Descriptions:
In this application you can:
1. Collect data of asset
2. Collect user details
3. Checklist associated with the asset
4. Surveying of the asset performance

Tools:
Android Studio, Android SDK

Libraries:
1. Kotlin
2. CodeScanner
3. Android material design
4. Coroutine
5. Retrofit
6. Kodein 

Business Logic :
1. Network API calls are performed using Retrofit Library
2. MVVM design pattern is followed
3. Kodein framework is used to perform dependency injection
4. Data are locally stored using Shared preferences
5. Network and Database operations are performed on Corountine rather than a worker thread



